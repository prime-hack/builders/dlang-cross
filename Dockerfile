FROM library/archlinux

RUN pacman -Syu --noconfirm && pacman -S --noconfirm git dub lld dlang-ldc && rm -r /var/cache/pacman/pkg/

COPY ldc2.conf /etc/
COPY lib32 /usr/ldc-windows-msvc/lib32
COPY lib64 /usr/ldc-windows-msvc/lib64
