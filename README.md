## Build

Place here dirs `lib32` and `lib64` from [ldc-windows-multilib](https://github.com/ldc-developers/ldc/releases), before building a container

#### build container

```bash
docker build -t dlang-cross .
```

## Usage

for compile dub-project in container

```bash
docker run -it -v source:source dlang-cross /bin/bash
cd /source
dub build --compiler=ldc2 --arch=i686-pc-windows-msvc
```

